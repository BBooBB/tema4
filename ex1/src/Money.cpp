#include "Money.h"
Money::Money(){}
Money::Money(int _euros, int _centimes)
{
	this->euros = _euros;
	this->centimes = _centimes;
}
int Money::getEuros()
{
	return this->euros;
}
int Money::getCentimes()
{
	return centimes;
}
void Money::setEuros(int _euros)
{
	this->euros = _euros;
}
void Money::setCentimes(int _centimes)
{
	this->centimes = _centimes;
}
void Money::print()
{
	cout << euros << "," << centimes << " euros" << endl;
}
Money Money::operator +(Money m1)
{
	Money rezultat;
	rezultat.euros = this->euros + m1.euros;
	rezultat.centimes = this->centimes + m1.centimes;
	if (rezultat.centimes > 100)	//daca dupa adunare sunt mai multi de 100 de centi, acestia sa se transforme in euro
	{
		rezultat.euros++;
		rezultat.centimes = rezultat.centimes - 100;
	}
	return rezultat;
}
Money Money::operator -(Money m1)
{
	Money rezultat;
	if (this->centimes - m1.centimes < 0)	//daca la scadere numarul de centi din deimpartit < numarul de centi din impartitor, sa se transforme un euro in 100 centi pentru deimpartit
	{
		this->centimes += 100;
		this->euros -= 1;
	}
	rezultat.euros = this->euros - m1.euros;
	rezultat.centimes = this->centimes - m1.centimes;
	return rezultat;
}
Money Money::operator /(Money m1)
{
	Money rezultat;
	rezultat.euros = this->euros / m1.euros;
	rezultat.centimes = this->centimes / m1.centimes;
	return rezultat;
}
Money Money::operator *(Money m1)
{
	Money rezultat;
	rezultat.euros = this->euros * m1.euros;
	rezultat.centimes = this->centimes * m1.centimes;
	return rezultat;
}
bool Money::operator !=(Money m1)
{
	if (this->euros != m1.euros || this->centimes != m1.centimes)
		return true;
	return false;
}
bool Money::operator ==(Money m1)
{
	if (this->euros == m1.euros && this->centimes == m1.centimes)
		return true;
	return false;
}


