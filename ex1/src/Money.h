#pragma once
#include <iostream>
using namespace std;
class Money
{
private:
	int euros, centimes;
public:
	Money();
	Money(int _euros, int _centimes);
	int getEuros();
	int getCentimes();
	void setEuros(int _euros);
	void setCentimes(int _centimes);
	void print();
	Money operator +(Money m1);
	Money operator -(Money m1);
	Money operator /(Money m1);
	Money operator *(Money m1);
	bool operator !=(Money m1);
	bool operator ==(Money m1);
};