#pragma once
#include<iostream>
#include<string>
#include<map>
#include "Money.h"
class CreditCard
{
private:
	std::string ownerName;
	std::string cardNumber;
	map<std::string ,Money>transactions;	//<itemName,cost>
public:
	CreditCard(std::string _name, std::string _cardNumber, map<std::string, Money> _transactions);
	void print();
	void charge_Money(std::string itemName, Money cost);
	void charge_int(std::string itemName, int euros, int centimes);
};