#include "CreditCard.h"
void main()
{
	map<std::string, Money> transactions;
	transactions.insert(pair<string, Money>("papuci",Money(200,0) ));
	transactions.insert(pair<string, Money>("bluza", Money(50,85)));
	transactions.insert(pair<string, Money>("mouse", Money(9,65)));
	CreditCard card("Ana","1010111875328409",transactions);
	card.print();
	cout << "Afisare dupa introducere cu cele doua functii charge:" << endl;
	card.charge_Money("poseta", Money(25, 50));
	card.charge_int("wiiU", 235, 87);
	card.print();
}