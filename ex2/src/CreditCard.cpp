#include "CreditCard.h"
CreditCard::CreditCard(string _name, const string _cardNumber, map<string, Money> _transactions)
{
	this->ownerName = _name;
	this->cardNumber = _cardNumber;
	this->transactions = _transactions;
}
void CreditCard::print()
{
	for (const auto& it : transactions)
	{
		cout << it.first<<" "<< it.second << endl;
	}
}
void CreditCard::charge_Money(string itemName, Money cost)
{
	transactions.insert(pair<string, Money>(itemName, cost));
}
void CreditCard::charge_int(string itemName, int euros, int centimes)
{
	Money cost;
	cost.setEuros(euros);
	cost.setCentimes(centimes);
	charge_Money(itemName, cost);	//dupa ce se formeaza variabila cost de tipul Money se apeleaza functia anterioara
									//mai putea fi scris astfel: charge_Money(itemName,Money(euros,centimes));
}