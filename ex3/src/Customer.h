#pragma once
#include<iostream>
#include<string>
using namespace std;

class Customer
{
	
private: 
	 string name;
	 bool member;
	 string memberType;
public:
	 Customer(string);
	 Customer();
	 Customer getCustomer(string);
	 string getName();
	 bool isMember();
	 void setMember(bool);
	 string getMemberType();
	 void setMemberType(string);
	 string toString();
};