#pragma once
#include "Customer.h"
#include "Date.h"
#include "Discount.h"
#include<iostream>
#include<string>

using namespace std;


class Visit
{
private:
	Customer customer;
	Date date;
	double serviceExpense;
	double productExpense;
public:
	Visit(Customer, Date);
	Visit();
	string getName();
	double getServiceExpense();
	void setServiceExpense(double);
	double getProductExpense();
	void setProductExpense(double);
	double getTotalExpense();
	string toString();
};