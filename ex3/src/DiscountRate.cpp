#include "Discount.h"

const double DiscountRate::getServiceDiscountRate(string type)
{
	if (type.compare("Premium")==0)
	{
		return this->serviceDiscountPremium;
	}
	if (type.compare("Gold") == 0)
	{
		return this->serviceDiscountGold;
	}
	if (type.compare("Silver") == 0)
	{
		return this->serviceDiscountSilver;
	}
}
const double DiscountRate::getProductDiscountRate(string type)
{
	if (type.compare("Premium") == 0)
	{
		return this->productDiscountPremium;
	}
	if (type.compare("Gold") == 0)
	{
		return this->productDiscountGold;
	}
	if (type.compare("Silver") == 0)
	{
		return this->productDiscountSilver;
	}
}