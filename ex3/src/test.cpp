#include<iostream>
#include<vector>
#include"Customer.h"
#include "Discount.h"
#include "Visit.h"
#include "Date.h"
using namespace std;

Customer getCustomer(string name,vector<Customer> v)
{
	for (Customer c:v)
		if (c.getName().compare(name) == 0)
			return c;
}

void main()
{
	Customer *premium = new Customer("Georgel");
	premium->setMember(true);
	premium->setMemberType("Premium");
	Customer *gold = new Customer("Ionel");
	gold->setMember(true);
	gold->setMemberType("Gold");
	Customer *silver = new Customer("Sandel");
	silver->setMember(true);
	silver->setMemberType("Silver");

	vector < Customer > v;
	v.push_back(*premium);
	v.push_back(*gold);
	v.push_back(*silver);

	Visit *v1 = new Visit(getCustomer("Sandel", v), Date("12", "03", "2017"));
	Visit *v2 = new Visit(getCustomer("Ionel", v), Date("10", "02", "2017"));
	Visit *v3 = new Visit(getCustomer("Georgel", v), Date("03", "01", "2017"));
	Visit *v4 = new Visit(getCustomer("Georgel", v), Date("18", "01", "2017"));

	v1->setProductExpense(200);
	v1->setServiceExpense(500);
	v2->setProductExpense(100);
	v2->setServiceExpense(600);
	v3->setProductExpense(300);
	v3->setServiceExpense(400);
	v4->setProductExpense(800);
	v4->setServiceExpense(600);

	cout << v1->toString() << "\n" << v2->toString() << "\n" << v3->toString() << "\n" << v4->toString() << "\n";

	system("Pause");

}