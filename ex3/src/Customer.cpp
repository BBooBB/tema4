#include "Customer.h"

Customer::Customer(string name)
{
	this->member = false;
	this->name = name;
}
Customer::Customer()
{

}
string Customer::getName()
{
	return this->name;
}
bool Customer::isMember()
{
	return this->member;
}
void Customer::setMember(bool member)
{
	this->member = member;
}
string Customer::getMemberType()
{
	return this->memberType;
}
void Customer::setMemberType(string memberType)
{
	this->memberType = memberType;
}
string Customer::toString()
{
	string s = "Name: " + this->name + "\nMember Type: " + this->memberType;
	return s;
}